# %%
#%pip uninstall pytube

# %%
#%pip install bs4 requests pytube3

# %%
from pytube import Playlist
from pytube import YouTube
import ffmpeg

# %%
import os

VIDEO_URL = os.environ['VIDEO_URL']

# %%
os.makedirs('data', exist_ok=True)

yt = YouTube(VIDEO_URL)
print(yt.title)
path = os.path.join('data', yt.title)
os.makedirs(path, exist_ok=True)
streams =  yt.streams.filter(only_audio=True)
itag = None
for s in streams:
    if s.mime_type == 'audio/mp4' and s.abr=='128kbps':
        itag = s.itag
if not itag:
    raise Exception("Not found audio/mp3 128kbps")

download_file_name = os.path.join(path, 'audio.mp4')
if not os.path.exists(download_file_name):
    d = yt.streams.get_by_itag(int(itag))
    p = d.download(filename=download_file_name)
    print('Downloaded', yt.title)
    hls = os.path.join(path, 'hls')
    os.makedirs(hls, exist_ok=True)

    input_stream = ffmpeg.input(download_file_name, f='mp4')
    output_stream = ffmpeg.output(input_stream, os.path.join(hls, 'audio.m3u8'), format='hls', start_number=0, hls_time=5, hls_list_size=0)

    ffmpeg.run(output_stream)
    print('Convert to HLS ok', yt.title)
