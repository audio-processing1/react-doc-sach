# %%
#%pip uninstall pytube

# %%
#%pip install bs4 requests pytube3

# %%
from pytube import Playlist

# %%
playlist = Playlist('https://www.youtube.com/watch?list=PLd7oGuDX6k1AqIbsG1nMUYxKbWykqzCz9')
print('Number of videos in playlist: %s' % len(playlist.video_urls))

# %%
import os

# %%
os.makedirs('data', exist_ok=True)

chapters = []

config = """
include: ".gitlab/**.yml"

stages:
  - build-package
  - test-package
  - publish-package
  - build
  - build-document
  - crawl
  - review-document
  - test
  - integration
  - deploy

variables:
  DEPLOY_PRODUCTION_SERVICE_NAME: ""
  PIPELINE_NAME: ""
  BUILD_ENV: ""
  BUILD_ALL_IMAGES: ""

workflow:
  name: '$PIPELINE_NAME'
  rules:
  - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
    variables:
      BUILD_ENV: merge-request
      PIPELINE_NAME: '#Build $GITLAB_USER_LOGIN - $CI_MERGE_REQUEST_TITLE'

  - if: "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
    variables:
      BUILD_ENV: merge-to-main
      PIPELINE_NAME: '#Deploy [DEV] # $GITLAB_USER_LOGIN - $CI_COMMIT_TITLE'

default:
  image: docker:dind

"""

for i, url in enumerate(playlist):
    with open('./video-template.yml', 'r') as f:
        data = f.read()
    data = data.replace('$video-url', f'"{url}"').replace('$order', str(i + 1))
    config += data + '\n\r'

with open('crawl-video.yml', 'w') as f:
    f.write(config)
