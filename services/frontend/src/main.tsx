import React, { useRef, useState } from 'react'
import ReactDOM from 'react-dom/client'
import { getAllBooks } from './api/book'
import { IChapter } from './api/chapter'
import './index.scss'
import AudioPlayer from 'react-h5-audio-player'
import 'react-h5-audio-player/lib/styles.css'
import ReactHlsPlayer from 'react-hls-player';

// /audio-streaming/49315cfbc3ff465e8029e1065fe7d09e/tom-tat-sach-day-con-lam-giau-tap-13-nang-cao-chi-so-iq-tai-chinh.m3u8

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
)

function App() {
  const [chapters] = useState<IChapter[]>(getAllBooks()[0].chapters);
  const [selectedChapter, setSelectedChapter] = useState<IChapter | undefined>();

  return (
    <div className="px-10 mt-10">
      <ListChapters
        chapters={chapters}
        onChapterClicked={(c) => setSelectedChapter(c)}
      />
      {selectedChapter &&
        <ChapterAudioPlayerContainer
          chapter={selectedChapter}
        />
      }
    </div>
  )
}

function ChapterItem(props: { chapter: IChapter, onClick?(): void }) {
  return (
    <div
      className="flex flex-row py-3 px-3 items-center gap-x-3 bg-gray-200 rounded-lg overflow-hidden"
      onClick={props.onClick}
    >
      <div>{props.chapter.id}</div>
      <div className="flex-grow">{props.chapter.name}</div>
      <div>
        <button className="bg-orange-500 px-3 py-2 text-white font-bold">
          Free
        </button>
      </div>
    </div>
  )
}

function ListChapters(props: { chapters: IChapter[], onChapterClicked?(c: IChapter): any }) {
  return (
    <div className="flex flex-col gap-y-2">
      {props.chapters.map(chapter => <ChapterItem onClick={() => props.onChapterClicked?.(chapter)} chapter={chapter} />)}
    </div>
  )
}

function ChapterAudioPlayerContainer(props: { chapter: IChapter }) {
  const [audioStorage] = useState<string>('http://27.71.27.89:81/data/%5BT%23U00f3m%20T%23U1eaft%20S%23U00e1ch%5D%20D%23U1ea1y%20Con%20L%23U00e0m%20Gi%23U00e0u%20%23U2013%20T%23U1eadp%202%3A%20S%23U1eed%20D%23U1ee5ng%20%23U0110%23U1ed3ng%20V%23U1ed1n%20%23U0110%23U1ec3%20%23U0110%23U01b0%23U1ee3c%20Tho%23U1ea3i%20M%23U00e1i%20V%23U1ec1%20Ti%23U1ec1n%20B%23U1ea1c/audio.mp4/%5BT%23U00f3m%20T%23U1eaft%20S%23U00e1ch%5D%20D%23U1ea1y%20Con%20L%23U00e0m%20Gi%23U00e0u%20%23U2013%20T%23U1eadp%202%20S%23U1eed%20D%23U1ee5ng%20%23U0110%23U1ed3ng%20V%23U1ed1n%20%23U0110%23U1ec3%20%23U0110%23U01b0%23U1ee3c%20Tho%23U1ea3i%20M%23U00e1i%20V%23U1ec1%20Ti%23U1ec1n%20B%23U1ea1c.mp4')
  const playRef = useRef(null);

  return (
    <div className="fixed bottom-0 left-0 right-0 grid grid-cols-3 bg-gradient-to-t from-gray-700 to-gray-600 text-white py-[20px]">
      <div className="flex flex-row items-center py-[10px]">
        <img className="w-[32] h-[32]" src="abc" />
        <div className="flex flex-col">
          <span>{props.chapter.name}</span>
          <span>Tác giả ai đó k biết</span>
        </div>
      </div>
      <div>
        <ReactHlsPlayer
          src="/audio-streaming/49315cfbc3ff465e8029e1065fe7d09e/tom-tat-sach-day-con-lam-giau-tap-13-nang-cao-chi-so-iq-tai-chinh.m3u8"
          autoPlay={false}
          controls={true}
          width="100%"
          height="auto"
          playerRef={playRef}
        />
      </div>
      <div>

      </div>
    </div>
  )
}
