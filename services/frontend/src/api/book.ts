import { IChapter } from "./chapter";
import bookData from './data/db.json' assert { type: 'json' }

export interface IBook {
  id: number;
  name: string;

  chapters: IChapter[];
}

export function getAllBooks(): IBook[] {
  return bookData;
}
