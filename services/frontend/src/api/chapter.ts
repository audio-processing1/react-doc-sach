
export interface IChapter {
  id: number;
  name: string;
  audioStorage: string;
}
