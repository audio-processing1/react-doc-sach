/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: [
          '"Inter var", sans-serif'
        ]
      },
      keyframes: {
        "scale-anim-keyframes": {
          '0%': {
            scale: '0.5'
          },
          '100%': {
            scale: '1'
          }
        }
      },
      animation: {
        "scale-anim": "scale-anim-keyframes 0.05s ease-in-out 1"
      }
    },
  },
  plugins: [],
}

